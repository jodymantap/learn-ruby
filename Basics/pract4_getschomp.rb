# gets.chomp for Simplw calculator

print "What year is it? "
currentYear = gets.chomp.to_i

print "What month is it? "
currentMonth = gets.chomp.to_i

print "What year you were born? "
yearOfBirth = gets.chomp.to_i

print "What month you were born ?"
monthOfBirth = gets.chomp.to_i


years = currentYear - yearOfBirth
months = currentMonth - monthOfBirth

if months < 0 
    years = years - 1
    months = 12 + months
end

puts "Alright! Your age is now #{years} years and #{months} months"